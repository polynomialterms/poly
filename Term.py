class Term:

    def __init__(self, degree, coeff):
        self.degree = degree
        self.coeff = coeff

    def __init__(self):
        self.degree = 0
        self.coeff = 0

    def add(self, term : Term):
        res = Term()
        if self.degree == self.degree:
            res.coeff = self.coeff + term.coeff
        return res

    def multiply(self, term : Term):
        res = Term()
        res.degree = self.degree + term.degree
        res. coeff = self.coeff * term.coeff
        return res

    def subtract(self, term):
        res = Term()
        if term.degree == self.degree:
            res.degree = self.degree
            res.coeff = self.coeff - term.coeff
        return res

    def differentiate(self):
        res = Term()
        res.coeff = self.degree * self.coeff
        res.degree = 0 if self.degree - 1 < 0 else self.degree - 1
        return res

class Polynomial:

    def __init__(self, terms : {int : Term}):
        self. terms = terms

    def __init__(self):
        self.terms = {}

    def add(self, poly : Polynomial):
        result = Polynomial()
        result.terms = self.terms
        for deg in poly.terms.keys():
            if deg not in result.terms:
                result.terms[deg] = poly.terms[deg]
            else:
                result.terms[deg] = result.terms[deg].add(poly.terms[deg])
        return result

    def subtract(self, poly : Polynomial):
        result = Polynomial()
        result.terms = self.terms
        for deg in poly.terms.keys():
            if deg not in result.terms:
                result.terms[deg] = poly.terms[deg]
            else:
                result.terms[deg] = result.terms[deg].subtract(poly.terms[deg])
        return result

    def differentiate(self):
        result = Polynomial()
        for deg in self.terms.keys():
            diffterm = self.terms[deg].differentiate()
            result[diffterm.degree] = diffterm
        return result

    def multiply(self, poly : Polynomial):
        result = Polynomial()
        for deg1 in self.terms.keys():
            for deg2 in poly.terms.keys():
                if (deg1 + deg2) not in result.terms:
                    result.terms[deg1 + deg2] = self.terms[deg1].multiply(poly.terms[deg2])
                else:
                    result.terms[deg1 + deg2] = result.terms[deg1 + deg2].add(self.terms[deg1].multiply(poly.terms[deg2]))
        return result
